public class Receptionists extends EmployeeSalaries{
    public Receptionists(int salary){
        super(salary);
    }

    public int getSalary() {
        return sal;
    }

    public void setSalary(int salary) {
        this.sal=salary;
    }
    public void CalculateSalary(){
        System.out.println("The salary of Receptionists is somewhere around " + sal);
    }
}