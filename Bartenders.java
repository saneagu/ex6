public class Bartenders extends EmployeeSalaries {
    private int salary;

    public Bartenders(int salary) {
        super(salary);
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void CalculateSalary() {
        System.out.println("The salary of Bartenders is somewhere around " + sal);
    }
}